const jwt = require('jsonwebtoken');

class AuthMiddleware {
    constructor() {
        
    }

    // Check auth of user
    auth({ req, res, next }) {
        const { headers } = req;
        const token = headers.authorization;

        
        if(!token) {
            return next({
                message: 'Unauthorization',
                data: null
            });
        }

        jwt.verify(token, Env.APP_KEY, (err, decoded) => {
            if(err) {
                return res.json({
                    message: 'err decode token',
                    data: null
                })
            }
            req.userId = decoded.userId;
            next();
        }) 
    }

    noAuth() {
        // TODO No Auth
        // Không cần phải đăng nhập vẫn có 1 số chức năng.
    }
    
}

module.exports = new AuthMiddleware();
