const UserModel = require('../../Models/UserModel')
const TokenModel = require('../../Models/TokenModel')
const { encode, generateToken } = require("../../Helpers/AuthHelpers");
const jwt = require('jsonwebtoken')

class UserController {
    constructor(){
        this.userModel = UserModel;
    }
    async getUserById({ req, res, next }) {
        const userId = req.params.id;

        
        const userFromDb = await this.userModel
          .query()
          .where({ id: userId })
          .first();
        if (!userFromDb) {
          next({
            message: "can_not_find_user",
            data: null
          });
        }
        
    
        const data = {
          username: userFromDb.username,
          name: userFromDb.name
        };
    
        res.status(200).json({
          message: "success",
          data
        });
    }
    async updateUserLogin({req,res,next}){
        const userId =req.userId;
        const dataUpdate =req.body;

        const userToUpdate = await this.userModel
            .query()
            .where({ id:userId })
            .update({ name : dataUpdate.name});

        if(!userToUpdate){
            return next({
                message :"can_not_find_user",
                data:null
            });
        }
        res.status(200).json({
            message :"update_success",
            data:null
        });
    }
    

}
module.exports = new UserController();  