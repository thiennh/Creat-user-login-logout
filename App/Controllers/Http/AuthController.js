const UserModel = require('../../Models/UserModel')
const TokenModel = require('../../Models/TokenModel')
const { encode, generateToken } = require("../../Helpers/AuthHelpers");
const jwt = require('jsonwebtoken')
class AuthController{
    constructor(){
        this.userModel = UserModel;
        this.tokenModel = TokenModel;
    }

    async insertTokenToDb({ userId, token }) {
        const dataTokenInsert = {
          user_id: userId,
          token,
          status: true
        };
    
        await this.tokenModel.query().insert(dataTokenInsert);
    }

    async register({ req, res, next }){
        const{body}=req;
        
        if(!body.username || !body.password){
            return res.json({
                message : 'username_or_password_is_require',
                data: null
            })
        }
        const user = await this.userModel
        .query().where('username',body.username).first();

        if(user){
            return res.json({
                message:'user_is_exit',
                data:null
            })
        }
        //Step 5
        //TODO
        const password = encode(body.password);
        const dataInsert ={
            username : body.username,
            password ,
            name:body.username
        };
        const userInserted = await this.userModel
            .query()
            .insert(dataInsert);

        let token = jwt.sign({
            id : userInserted.id,
            timestamps : new Date().getTime()
        },Env.APP_KEY)
        const dataTokenInsert = {
            user_id : userInserted.id,
            token : token,
            status : 1
        }
        await this.tokenModel
            .query()
            .insert(dataTokenInsert);

        return res.json({
            message:'register_success',
            data: token.value
        })
    }
    async login({req,res,next}){
        const{body} =req;
        if(!body.username || !body.password){
            return next({
                message : "username_or_password_is_request",
                data: null
            });
        }
        const user = await this.userModel
            .query()
            .where({username:body.username})
            .first();


        if(!user){
            return next({
                message: "user_is_wrong"
            });
        }
        if (user.password !== encode(body.password)) {
            return next({
              message: "password_is_wrong",
              data: null
            });
        }
        const token = generateToken(user.id);
        await this.insertTokenToDb({userId:user.id,token});

        return res.status(200).json({
            message: "login_success",
            data: { token }
        });
    }
    async logout({ req, res, next }) {
        const { headers } = req;
        const token = headers.authorization;
        if (!token) {
            return res.json({
                message: 'No_exits!',
                data: null
            });
        }
        await this.tokenModel
            .query()
            .where('token', token)
            .first()
            .del();
        {
        return res.json({
            message: 'Logout success!!!',
            data: null
        });
        }
    } 

}

module.exports = new AuthController();