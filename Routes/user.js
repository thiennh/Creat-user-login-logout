const express = require("express");

const authMiddleware = require("../App/Middlewares/AuthMiddleware");

const userController = require("../App/Controllers/Http/UserController");

const router = express.Router();

router.use((req, res, next) => {
  authMiddleware.auth({ req, res, next });
});

router
  .route("/:id")
  .get((req, res, next) => {    userController.getUserById({ req, res, next });
  })
  .put((req, res, next) => {
    userController.updateUserLogin({ req, res, next });
  });

module.exports = router;