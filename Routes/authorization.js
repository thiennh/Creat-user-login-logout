const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const AuthController = require('../App/Controllers/Http/AuthController')

const router = express.Router();

const users = [];
router.post('/register', (req, res, next) => {
    AuthController.register({ req, res, next});
})

router.post('/login', (req, res, next) => {
    AuthController.login({req,res,next});
})
router.post('/logout', (req, res, next) => {
    AuthController.logout({req,res,next});
})

module.exports = router;
