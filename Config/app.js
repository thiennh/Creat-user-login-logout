const express = require('express');
require('dotenv').config();

global.Env = process.env;

const app = express();

const http = require('http').Server(app);

app.use(express.json());

http.listen(Env.PORT, () => {
    console.log(`Port ${Env.PORT}`);
});

module.exports = app;
